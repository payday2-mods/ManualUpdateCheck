
-- surpress auto updates at start
BLT.Mods._has_checked_for_updates = true

local init_managers_original = MenuSetup.init_managers
function MenuSetup:init_managers(...)
	init_managers_original(self, ...)

	-- create initial notification
	if BLT.Mods._has_checked_for_updates then
		local icon, rect = tweak_data.hud_icons:get_icon_data("csb_pagers")
		BLT.Mods._updates_notification = BLT.Notifications:add_notification({
			title = "Welcome!",
			text = "Right-click here to check for updates.",
			icon = icon,
			icon_texture_rect = rect,
			color = Color.white,
			priority = 0,
		})
	end
end

local mouse_pressed_original = BLTNotificationsGui.mouse_pressed
function BLTNotificationsGui:mouse_pressed(button, x, y, ...)

	if not self._enabled then
		return
	end

	if button == Idstring("1") and alive(self._content_panel) and self._content_panel:inside(x, y) then

		-- cleanup old notification
		if BLT.Mods._updates_notification then
			BLT.Notifications:remove_notification(BLT.Mods._updates_notification)
			BLT.Mods._updates_notification = nil
		end

		-- trigger update check
		--BLT.Mods._has_checked_for_updates = false
		--BLT.Mods:RunAutoCheckForUpdates()
		call_on_next_update(callback(BLT.Mods, BLT.Mods, "_RunAutoCheckForUpdates"))

		return true
	else
		return mouse_pressed_original(self, button, x, y, ...)
	end

end

local clbk_got_update_original = BLTModManager.clbk_got_update
function BLTModManager:clbk_got_update(...)
	clbk_got_update_original(self, ...)

	-- force refresh (fix for the notification gui being retarded sometimes)
	local notifications = (managers.menu_component.blt_notifications_gui and managers.menu_component:blt_notifications_gui()) or (managers.menu_component.blt_notifications and managers.menu_component:blt_notifications())
	if notifications then
		notifications:_update_bars()
	end
end